<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Customer;
use Validator;

class CustomerController extends Controller
{
  // render add customer viewAdmin
  public function  addCustomer()
  {

     return view('customer.addCustomer');
  }

  // Save Customer
  public function  saveCustomer(Request $request)
  {
    Validator::make(
                       $request->all(),
                       ['name'=>'required',
                       'email'=>'required|email',
                       'phone'=>'required |numeric',
                       'pwd'=>'required',
                       'status'=>'required',
                       'address'=>'required',
                       ]
    )->validate();

    $customer=new Customer();
    $customer->name=$request->name;
    $customer->email=$request->email;
    $customer->phone_number=$request->phone;
    $customer->password=$request->pwd;
    $customer->status=$request->status;
    $customer->address=$request->address;

     $customer->save();

     return redirect()->back()->with('success', 'Successfully   registered');

  }

  // View All  customer

  public function viewcCustomer()
  {
      return view('customer.viewCustomer',['customers'=>Customer::all()]);
  }
  public function customerSignup(Request $request){

    if(!empty($request->name) && !empty($request->email) && empty(Customer::where('email','=',$request->email)->get()->toArray()) && !empty($request->password)) {
        $customer=new Customer();
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone_number = isset($request->phone)? $request->phone :'';
        $customer->password = $request->password;
        $customer->status = 1;
        $customer->address = isset($request->address)? $request->address: '';

        try{
          $customer->save();
          return response()->json(['message' =>'Signed Up Successfully','data'=>$customer,'response_code'=>1], 200);
        }catch (\Exception $e){
            return response()->json(['message' =>$e->getMessage(),'response_code'=>0], 500);
        }
    }else{
        return response()->json(['message' =>'Sign Up Failed','response_code'=>0], 500);
    }
  }

  public function customerLogin(Request $request){

    if(!empty($request->email) && !empty($request->password)) {
        try{
          $customer = Customer::where('email','=',$request->email)
                      ->where('password','=',$request->password)
                      ->first();
          if(!empty($customer))
            return response()->json(['message' =>'Customer Found','data'=>$customer,'response_code'=>1], 200);
          else
            throw new Exception('Customer Not Found');
        }catch (\Exception $e){
            return response()->json(['message' =>$e->getMessage(),'response_code'=>0], 500);
        }
    }else{
        return response()->json(['message' =>'Sign Up Failed','response_code'=>0], 500);
    }
  }

 /*
 / social login e.g facebook,gmail etc
 /$uid  userid from facebook or gmail etc
 / $env enviroment  keyword e.g facebook, gmail etc
 */
  public function socialLogin(Request $request)
  {
    
    try{
      $customer = Customer::where('uid','=',$request->input('uid'))
                  ->where('enviroment','=',$request->input('env'))
                  ->first();
      if(!empty($customer))
        return response()->json(['message' =>'Customer Found','data'=>$customer,'response_code'=>1], 200);
      else
        return response()->json(['message' =>'Customer Not Found','data'=>$customer,'response_code'=>1], 200);
        //throw new Exception('Customer Not Found');
    }catch (\Exception $e){
        return response()->json(['message' =>$e->getMessage(),'response_code'=>0], 500);
    }
  }

}
