<?php

namespace App\Http\Controllers;
use Validator;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\ownclasses\Custom;
//use App\Admin;
class AdminController extends Controller
{
   public function index()
   {
    //  echo url('/');
      return view('admin.index');
   }
   public function dashboard()
   {
      return view('admin.dashboard');
   }
 // VIEW to add new Admin
   public function addAdmin()
   {
        return view('admin.addAdmin');
   }

 // saving new admin into DB
   public function saveAdmin( Request $request)
   {

     Validator::make(
                        $request->all(),
                        ['name'=>'required',
                        'uname'=>'required',
                        'pwd'=>'required',
                        'email'=>'required|email',
                        'admintype'=>'required',
                        'image'=>'required'
                        ]
     )->validate();

           $extension=$request->image->extension();
           $custom=new Custom();
           if ($custom->isImage($extension)) {
             $admin=new Admin();
              $admin->name=$request->name;
              $admin->username=$request->uname;
              $admin->password=$request->pwd;
              $admin->email=$request->email;
              $admin->admin_type=$request->admintype;

              $admin->image=Storage::url( Storage::putFile('public/images',$request->image));

              $admin->save();

              return redirect()->back()->with('success', 'Successfully   registered');


           }
           else {
              return redirect()->back()->with('error', 'only image is accepted');
           }

     return redirect()->back();
   }

   // display all registered admins
   public function viewAdmin()
   {
      $admin=Admin::all();
    //  dd($admin);
     return view('admin.viewAdmin',['users'=>$admin]);
   }

  // show specific admin to edit
   public function editAdmin($id)
   {
          $admin=Admin::where('id','=',$id)->get();
          return view('admin.editAdmin',['user'=>$admin]);
   }
   // update admin information
   public function updateAdmin(Request $request)
   {
          $data=[
                  'name'=>$request->name,
                  'username'=>$request->uname,
                  'email'=>$request->email,
                  'admin_type'=>$request->admintype,
                ];
          $admin=Admin::where('id','=',$request->id)->update($data);
          return redirect()->route('viewAdmin')->with('success', 'Successfully Updated');
   }

   public function deleteAdmin($id)
   {
     # code...
      return redirect()->back();
   }



}
