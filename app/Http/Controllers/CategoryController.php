<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\ownclasses\Custom;
use App\ownclasses\StoreImages;
use App\Category;
use Validator;
use Illuminate\Support\Facades\Storage;

use Image;
class CategoryController extends Controller
{
    // render category view
    public function addCategory()
    {
      $categories = Category::where('parent_category','=','0')->get();

      return view('category.addCategory',['categories'=>$categories]);
    }

    // Save category
    public function saveCategory(Request $request)
    {
      Validator::make($request->all(),['name'=>'required','photo'=>'required','p_category'=>'required'])->validate();
      $extension=$request->photo->extension();

      // custom class
      $custom=new Custom();
      $imageType=$custom->copyImage($extension);

      if ($custom->isImage($extension)) {

        $requestImagePath= $request->photo->getRealPath().'.'.$extension;
        $imageUrl=[];
        $count=0;
        foreach ($imageType as $key => $value) {
         $interventionImage= Image::make($request->photo)->resize($value['width'],$value['height']) ->encode($extension,$value['quality']);
         $interventionImage->save($requestImagePath);
         $imageUrl[$count]= url('/').Storage::url( Storage::putFileAs('public/images',new File($requestImagePath),$value['name']));
      //  Storage::url( Storage::putFileAs('public/images',new File($requestImagePath),$value['name']));
        // $imageUrl[$count] =$value['name'];
         $count++;
        }
      //$images=json_encode(array('large' =>$imageUrl[0],'medium' =>$imageUrl[1],'small' =>$imageUrl[2]));

      // $images=json_encode(array('large' =>$imageUrl[0],'medium' =>$imageUrl[1],'small' =>$imageUrl[2]),JSON_UNESCAPED_SLASHES);
      //  dd($images);
         $cateogry=new Category();
         $cateogry->name=$request->name;
         $cateogry->parent_category=$request->p_category;
      //   $cateogry->image_name=$images;
         $cateogry->is_featured=0;
         $cateogry->is_deleted=0;
         $cateogry->save();

         // save images into db
         $photo=new StoreImages();
         $photo->saveImage($imageUrl, $cateogry->id,1);

        // return $cateogry->id;
        return redirect()->back()->with('success', 'Successfully   added');
      }
      else {
         return redirect()->back()->with('error', 'only image is accepted');
      }

    }

    // display all category
     public function viewCategory()
     {
        return view('category.viewCategory',['categories'=>Category::where('parent_category','=',0)->get()]);
     }

   // GET All Categories
    public function getAllCategories()
    {
       $categories = Category::with('photo')->where('parent_category','=', 0)->get();
       return response()->json(['message' =>'success','response_code'=>1, 'data' =>$categories], 200);
    }

    // GET Featured Categories
     public function featuredCaterories()
     {
        $featuredCategories = Category::with('photo')->where('is_featured','=', 1)->get();
        return response()->json(['message' =>'success','response_code'=>1, 'data' =>$featuredCategories], 200);
     }


   // GET All subCategories
    public function subCategories()
    {
       $subCategories = Category::with('photo')->where('parent_category','<>', 0)->get();
       return response()->json(['message' =>'success','response_code'=>1, 'data' =>$subCategories], 200);
    }

    // GET All subCategories
     public function subCatByCategoryId($categoryId)
     {
        $subCategories = Category::with('photo')->where('parent_category','=', $categoryId)->get();
        return response()->json(['message' =>'success','response_code'=>1, 'data' =>$subCategories], 200);
     }


     // edit a specific category

     public function editCategory($id)
     {


         return view('category.editCategory',['category'=>Category::where('id','=',$id)->get()]);

     }

     // save edit category
     public function updateCategory(Request $request)
     {
      //  Validator::make($request->all(),['name'=>'required'])->validate();


      $data=['name'=>$request->name];
       Category::where('id','=',$request->id)->update($data);
        return redirect()->route('viewCategory')->with('success', 'Successfully updated');

     }
     // dislay all subCategory
     public function displaysubCategory()
     {
          return view('category.viewSubcategory',['categories'=>Category::where('parent_category','<>',0)->get()]);
     }

     /*
     /Edit subCategory
     */
     public function editSubcategory($id)
     {
        $sub_category=Category::where('id','=',$id)->get();

        $parent_category=Category::where('parent_category','=',0)->get();
        return view('category.editSubcategory',['category'=>$sub_category,'parentcategory'=>$parent_category]);
     }
     /*
     / UPDATE subCategory
     */
     public function updateSubcategory(Request $request)
     {
      //  Validator::make($request->all(),['name'=>'required'])->validate();


      $data=['name'=>$request->name,'parent_category'=>$request->p_category];
       Category::where('id','=',$request->id)->update($data);
        return redirect()->route('viewsubcategory')->with('success', 'Successfully updated');

     }
     // Delete Category

     public function deleteCategory($id)
     {
       return 'delete';
     }

     public function subCategory($cat_id){
        $sub_cat = Category::Where('parent_category','=',$cat_id)->get()->toArray();
        return json_encode($sub_cat);
     }

     /*
      Category is Featured check
     */
     public function isFeatured($id,$status)
     {
        if ($status==1) {
            Category::where('id','=',$id)->update(['is_featured'=>1]);
            return 'Enabled';
        }
        if ($status==0) {
            Category::where('id','=',$id)->update(['is_featured'=>0]);
            return 'Disabled';
        }

     }
     /*
       search category api
     */
     public function searchCategory($search){
       try{
         $category = Category::with('photo')->where('name','LIKE',"%{$search}%")->where('parent_category','=',0)->get();

         return response()->json(['message' =>'Category Found','data'=>$category,'response_code'=>1], 200);

       }catch (\Exception $e){
           return response()->json(['message' =>$e->getMessage(),'response_code'=>0], 500);
       }


     }

}
