<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\ownclasses\Custom;
use App\ownclasses\StoreImages;
use Validator;
use App\Category;
use App\Product;
use Image;

class ProductController extends Controller
{
     public function addProduct()
     {
        return view('product.addProduct',['category'=>Category::where('parent_category','=','0')->get()] );
     }

     public function saveProduct(Request $request)
     {
       Validator::make(
                          $request->all(),
                          ['name'=>'required',
                          'category'=>'required',
                          'cost_price'=>'required|numeric',
                          'sale_price'=>'required|numeric',
                        //  'merchant'=>'required',
                          'discount'=>'required|numeric',
                          'image'=>'required',
                          'desc'=>'required',
                          'specification'=>'required',
                          ]
       )->validate();

       // custom class
       $custom=new Custom();

       $imageUrl=[];
       for ($i=0; $i <count($request->image) ; $i++)
       {
        $extension=$request->image[$i]->extension();
        $imageType=$custom->copyImage($extension);
         if (!($custom->isImage($extension)))
         {

         }
         else
         {
           $requestImagePath= $request->image[$i]->getRealPath().'.'.$extension;

           $count=0;
           foreach ($imageType as $key => $value) {
            $interventionImage= Image::make($request->image[$i])->resize($value['width'],$value['height']) ->encode($extension,$value['quality']);
            $interventionImage->save($requestImagePath);
              $imageUrl[$count]= url('/').Storage::url( Storage::putFileAs('public/images',new File($requestImagePath),$value['name']));
          //  $imageUrl[$i][$count]=$value['name'];
            $count++;
           }

         }

       }
               // store product

                $product=new Product();
                $product->name=$request->name;
                $product->category_id= !empty($request->c_category) ? $request->c_category : $request->category;
                $product->cost_price=$request->cost_price;
                $product->sale_price=$request->sale_price;
                $product->merchant_id=1;
                $product->discount_perc=$request->discount;
                $product->desc=$request->desc;
                $product->specification=$request->specification;
                $product->save();

                // store images into db
               $photo=new StoreImages();
              $photo->saveImage($imageUrl, $product->id,0);

              return redirect()->route('viewProduct')->with('success', 'Successfully added');

     }

     // View All product
     public function viewProduct()
     {
        return view('product.viewProduct',['products'=>Product::all()]);
     }

     //Display a specific product to Edit Product
      public function editProduct($id)
      {
         return view('product.editProduct',['products'=>Product::where('id','=',$id)->get(),'category'=>Category::all()]);
      }

      // update product
      public function updateProduct(Request $request)
      {

      $data=[
          'name'=>$request->name,
         //'category_id'=>$request->category,
         'cost_price'=>$request->cost_price,
         'sale_price'=>$request->sale_price,
        //  'merchant_id'=>$request->merchant,
        'category_id'=>$request->category,
         'discount_perc'=>$request->discount,
         'desc'=>$request->desc,
         'specification'=>$request->specification
         ];

         Product::where('id','=',$request->id)->update($data);
         return  redirect()->route('viewProduct')->with('success', 'updated successfully');
      }

      // Delete a specific product
    public function deleteProduct($id)
    {
        Product::where('id','=',$id)->delete();
        return  redirect()->back()->with('success', 'deleted successfully');
    }

    public function productBySubCategoryId($subCatId)
    {
        $products = Product::with('photo')->where('category_id','=', $subCatId)->get();
        return response()->json(['message' =>'success','response_code'=>1, 'data' =>$products], 200);
    }

    public function productById($productId)
    {
        $product = Product::with('photo')->where('id','=', $productId)->first();
        if(empty($product))
          return response()->json(['message' =>'success','response_code'=>0, 'data' =>'no data found'], 200);
        else
          return response()->json(['message' =>'success','response_code'=>1, 'data' =>$product], 200);
    }


}
