<?php
namespace App\ownclasses;

class Custom{

  // checking upload file extension

  public function  isImage($extension)
  {
      switch ($extension) {
        case 'jpg':
            return true;
          break;
        case 'jpeg':
            return true;
            break;
        case 'png':
           return true;
           break;
         case 'gif':
              return true;
          break;

        default:
           return false;
          break;
      }
  }

  // Copy A upload image into 3 images

  public function copyImage($extension)
  {

    $imageType = array(
             'thumbnail' => array(
                 'width' => 500,
                 'height' => 500,
                 'name'  =>uniqid().time().'.'.$extension,
                 'quality'=>100,
             ),
             'detail_page' => array(
                 'width' => 250,
                 'height' => 250,
                  'name'  =>uniqid().time().'.'.$extension,
                   'quality'=>100,

             ),
             'product' => array(
                  'width' => 190,
                  'height' =>190,
                  'name'  =>uniqid().time().'.'.$extension,
                   'quality'=>100,

             ),
         );

         return $imageType;
  }

}
