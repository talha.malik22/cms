<?php
namespace App\ownclasses;
use App\Photo;
class StoreImages{


      public function saveImage($imageUrl,$id,$id_type)
      {
           /*
           // $id_type=0 mean save product images into db
            // $id_type=1 mean save  category or subcategory images into db
           */
          $photo=new Photo();
          if ($id_type==0) {
            $photo->product_id=$id;
          }
          elseif ($id_type==1) {
              $photo->category_id=$id;
          }

          $photo->image_large=$imageUrl[0];
          $photo->image_medium=$imageUrl[1];
          $photo->image_small=$imageUrl[2];
          $photo->save();
      }

}
