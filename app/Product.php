<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{

  protected $table = 'products';
  protected $fillable = ['id','name','category_id','cost_price','sale_price','desc','specification','merchant_id','discount_perc'];

  public function photo()
  {
     return $this->hasMany(Photo::class);
  }
}
