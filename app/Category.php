<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
      protected $table='categories';
      protected $fillable=['id','name','parent_category','is_featured','is_deleted'];

      public function photo()
      {
         return $this->hasMany(Photo::class);
      }
}
