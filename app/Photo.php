<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table="images";
    protected $fillable=['id','product_id','category_id','image_large','image_medium','image_small'];
    public function category()
    {
        return $this->hasOne(Category::class);
    }

    public function product()
    {
       return $this->hasOne(Product::class);
    }
}
