{{-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> --}}


					<!-- Load jQueryUI -->
<script type="text/javascript" src="{{asset('admin/assets/js/bootstrap.min.js')}}"></script> 								<!-- Load Bootstrap -->
<script type="text/javascript" src="{{asset('admin/assets/js/bootstrap-toggle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/enquire.min.js')}}"></script> 									<!-- Load Enquire -->

<script type="text/javascript" src="{{asset('admin/assets/plugins/velocityjs/velocity.min.js')}}"></script>					<!-- Load Velocity for Animated Content -->
<script type="text/javascript" src="{{asset('admin/assets/plugins/velocityjs/velocity.ui.min.js')}}"></script>

<script type="text/javascript" src="{{asset('admin/assets/plugins/wijets/wijets.js')}}"></script>     						<!-- Wijet -->

{{-- <script type="text/javascript" src="{{asset('admin/assets/plugins/codeprettifier/prettify.js')}}"></script> 				<!-- Code Prettifier  --> --}}
<script type="text/javascript" src="{{asset('admin/assets/plugins/bootstrap-switch/bootstrap-switch.js')}}"></script> 		<!-- Swith/Toggle Button -->

<script type="text/javascript" src="{{asset('admin/assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js')}}"></script>  <!-- Bootstrap Tabdrop -->

<script type="text/javascript" src="{{asset('admin/assets/plugins/iCheck/icheck.min.js')}}"></script>     					<!-- iCheck -->

<script type="text/javascript" src="{{asset('admin/assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js')}}"></script> <!-- nano scroller -->

<script type="text/javascript" src="{{asset('admin/assets/js/application.js')}}"></script>
{{-- <script type="text/javascript" src="{{asset('admin/assets/demo/demo.js')}}"></script> --}}
<script type="text/javascript" src="{{asset('admin/assets/demo/demo-switcher.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/custom.js')}}"></script>

<!-- End loading site level scripts -->

    <!-- Load page level scripts-->
