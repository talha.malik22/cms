<div class="static-sidebar-wrapper sidebar-default">
    <div class="static-sidebar">
        <div class="sidebar">
<div class="widget">
<div class="widget-body">
<div class="userinfo">
<div class="avatar">
    <img src="http://placehold.it/300&text=Placeholder" class="img-responsive img-circle">
</div>
<div class="info">
    <span class="username">Jonathan Smith</span>
    <span class="useremail">jon@avenxo.com</span>
</div>
</div>
</div>
</div>
<div class="widget stay-on-collapse" id="widget-sidebar">
<nav role="navigation" class="widget-body">
<ul class="acc-menu">
<li class="nav-separator"><span>Explore</span></li>
<li><a href="{{route('adminDashboard')}}"><i class="ti ti-home"></i><span>Dashboard</span><span class="badge badge-teal">2</span></a></li>

<li><a href="javascript:;"><i class="ti ti-pencil"></i><span>Admin</span></a>
<ul class="acc-menu">
<li><a href="{{route('addNewAdmin')}}">Add Admin</a></li>
<li><a href="{{route('viewAdmin')}}">View Admin</a></li>

</ul>

<li><a href="javascript:;"><i class="ti ti-pencil"></i><span>Customer</span></a>
<ul class="acc-menu">
<li><a href="{{route('addCustomer')}}">Add Customer</a></li>
<li><a href="{{route('viewCustomer')}}">View Customer</a></li>
</ul>
</li>

<li><a href="javascript:;"><i class="ti ti-pencil"></i><span>Category</span></a>
<ul class="acc-menu">
<li><a href="{{route('addCategory')}}">Add Category</a></li>
<li><a href="{{route('viewCategory')}}">View Category</a></li>
<li><a href="{{route('viewsubcategory')}}">View Subcategory</a></li>
</ul>
</li>
<li>

  <li><a href="javascript:;"><i class="ti ti-pencil"></i><span>Product</span></a>
  <ul class="acc-menu">
  <li><a href="{{route('addProduct')}}">Add Product</a></li>
  <li><a href="{{route('viewProduct')}}">View Product</a></li>
  {{-- <li><a href="{{route('editProduct')}}">Edit Product</a></li> --}}
  </ul>
  </li>
  <li>

</nav>
</div>


</div>
    </div>
</div>
