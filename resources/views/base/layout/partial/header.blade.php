
<header id="topnav" class="navbar navbar-default navbar-fixed-top" role="banner">

<div class="logo-area">
<span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
<a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
<span class="icon-bg">
  <i class="ti ti-menu"></i>
</span>
</a>
</span>
<a class="navbar-brand" href="#">i-Buy2U</a>
{{-- <a class="navbar-brand" href="#">i-Buy2U</a> --}}

{{-- <div class="toolbar-icon-bg hidden-xs" id="toolbar-search">
    <div class="input-group">
      <span class="input-group-btn"><button class="btn" type="button"><i class="ti ti-search"></i></button></span>
<input type="text" class="form-control" placeholder="Search...">
<span class="input-group-btn"><button class="btn" type="button"><i class="ti ti-close"></i></button></span>
</div>
</div> --}}

<script type="text/javascript" src="{{asset('admin/assets/js/jquery-1.10.2.min.js')}}"></script> 							<!-- Load jQuery -->
<script type="text/javascript" src="{{asset('admin/assets/js/jqueryui-1.10.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/assets/js/jquery.validate.min.js')}}"></script>                                     
{{-- <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"> --}}
<!-- Custom SCRIPT-->
@yield('custom-script')
<!-- end custom script -->
</div>
