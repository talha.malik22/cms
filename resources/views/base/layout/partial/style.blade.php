<link type='text/css' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600' rel='stylesheet'>

<link type="text/css" href="{{asset('admin/assets/css/bootstrap-toggle.min.css')}}" rel="stylesheet">
<link type="text/css" href="{{asset('admin/assets/fonts/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">        <!-- Font Awesome -->
<link type="text/css" href="{{asset('admin/assets/fonts/themify-icons/themify-icons.css')}}" rel="stylesheet">              <!-- Themify Icons -->
<link type="text/css" href="{{asset('admin/assets/css/styles.css')}}" rel="stylesheet">
<link type="text/css" href="{{asset('admin/assets/css/bootstrap-toggle.min.css')}}" rel="stylesheet">                                     <!-- Core CSS with all styles -->
<link type="text/css" href="{{asset('admin/assets/css/custom.css')}}" rel="stylesheet">                                     <!-- Core CSS with all styles -->

<link type="text/css" href="{{asset('admin/assets/plugins/codeprettifier/prettify.css')}}" rel="stylesheet">                <!-- Code Prettifier -->
<link type="text/css" href="{{asset('admin/assets/plugins/iCheck/skins/minimal/blue.css')}}" rel="stylesheet">              <!-- iCheck -->

<!--[if lt IE 10]>
    <script type="text/javascript" src="assets/js/media.match.min.js"></script>
    <script type="text/javascript" src="assets/js/respond.min.js"></script>
    <script type="text/javascript" src="assets/js/placeholder.min.js"></script>
<![endif]-->
<!-- The following CSS are included as plugins and can be removed if unused-->

<link type="text/css" href="{{asset('admin/assets/plugins/fullcalendar/fullcalendar.css')}}" rel="stylesheet"> 						<!-- FullCalendar -->
<link type="text/css" href="{{asset('admin/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet"> 			<!-- jVectorMap -->
<link type="text/css" href="{{asset('admin/assets/plugins/switchery/switchery.css')}}" rel="stylesheet">
