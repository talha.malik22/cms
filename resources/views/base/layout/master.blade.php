<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>@yield('title')</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="description" content="Avenxo Admin Theme">
  <meta name="author" content="KaijuThemes">
     <!-- style Section -->
       @include('base.layout.partial.style')
     <!-- End of style section -->
     <!-- Custom style -->
      @yield('custom-style')
     <!-- end custom style -->
    </head>

    <body class="animated-content">
     <!--  Header -->
        @include('base.layout.partial.header')
     <!-- end of Header -->

	     <!-- Navbar start -->
           @include('base.layout.partial.nav')
       <!-- end of navbar -->

<div id="wrapper">
    <div id="layout-static">
    <!---Sidebar --->
     @include('base.layout.partial.sidebar')
     <!-- end sidebar -->
        <div class="static-content-wrapper">
            <div class="static-content">
                <div class="page-content">
                  <!-- content -->
                   @section('content')

                   @show
                   <!-- end content -->

                </div> <!-- #page-content -->
            </div>



  <!-- footer -->
@include('base.layout.partial.footer')
  <!-- end of footer -->


<!-- Switcher -->
  @include('base.layout.partial.switcher')
<!-- /Switcher -->
    <!-- Load site level scripts -->

      <!-- script section -->
      @include('base.layout.partial.script')
      <!-- end of script section -->




    </body>
</html>
