
<!DOCTYPE html>
<html lang="en" class="coming-soon">
<head>
    <meta charset="utf-8">
    <title>Login Form</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="author" content="KaijuThemes">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600' rel='stylesheet' type='text/css'>

    @include('base.layout.partial.style')
    <script type="text/javascript" src="{{asset('admin/assets/js/jquery-1.10.2.min.js')}}"></script> 							<!-- Load jQuery -->
    <script type="text/javascript" src="{{asset('admin/assets/js/jqueryui-1.10.3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/jquery.validate.min.js')}}"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
    <!--[if lt IE 9]>
        <link type="text/css" href="assets/css/ie8.css" rel="stylesheet">
        <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The following CSS are included as plugins and can be removed if unused-->

    </head>

    <body class="focused-form animated-content">


<div class="container" id="login-form">
	<a href="index.html" class="login-logo"><img src="{{asset('admin/assets/img/buy2.jpg')}}"></a>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2>Login Form</h2>
					</div>
					<div class="panel-body">
            @if (session()->has('error'))
                <p  class="alert alert-danger">{{session('error')}}</p>
            @endif
            @if (session()->has('success'))
                <p  class="alert alert-success">{{session('success')}}</p>
            @endif
						<form action="{{route('authenticateUser')}}" class="form-horizontal" method="post" id="validate-form">
              {{csrf_field()}}
							<div class="form-group mb-md">
		                        <div class="col-xs-12">
		                        	<div class="input-group">
										<span class="input-group-addon">
											<i class="ti ti-user"></i>
										</span>
										<input type="text" class="form-control" name="uname" placeholder="Email" data-parsley-minlength="6" placeholder="At least 6 characters" >
									</div>
		                        </div>
							</div>

							<div class="form-group mb-md">
		                        <div class="col-xs-12">
		                        	<div class="input-group">
										<span class="input-group-addon">
											<i class="ti ti-key"></i>
										</span>
										<input type="password" class="form-control" name="pwd" id="exampleInputPassword1" placeholder="Password">
									</div>
		                        </div>
							</div>

							<div class="form-group mb-n">
								<div class="col-xs-12">
									<a href="extras-forgotpassword.html" class="pull-left">Forgot password?</a>
									<div class="checkbox-inline icheck pull-right p-n">
										<label for="">
											<input type="checkbox"></input>
											Remember me
										</label>
									</div>
								</div>
							</div>
              <div class="panel-footer">
    						<div class="clearfix">
    						<!--	<a href="extras-registration.html" class="btn btn-default pull-left">Register</a> -->
    							<button type="submit"  class="btn btn-primary pull-right">Login</button>
    						</div>
    					</div>

						</form>
					</div>

				<!--	<div class="panel-footer">
						<div class="clearfix">
							<a href="extras-registration.html" class="btn btn-default pull-left">Register</a>
							<button type="submit"  class="btn btn-primary pull-right">Login</button>
						</div>
					</div> -->
				</div>



		</div>
</div>




<!-- End loading site level scripts -->
    <!-- Load page level scripts-->

@include('base.layout.partial.script')
    <!-- End loading page level scripts-->
    </body>
</html>
