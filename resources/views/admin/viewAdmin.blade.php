@extends('base.layout.master')
@section('content')
  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="panel panel-default" data-widget='{"draggable": "false"}'>
        <div class="panel-heading">
          <h2>View Admin</h2>
          <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
          <div class="options">

          </div>
        </div>
        @if (session()->has('error'))
            <p  class="alert alert-danger">{{session('error')}}</p>
        @endif
        @if (session()->has('success'))
            <p  class="alert alert-success">{{session('success')}}</p>
        @endif
        <div class="panel-body">

          <table class="table table-bordered">
            <thead>
              <tr>

                <th>Name</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Admin Type</th>
                <th>Action</th>
              </tr>

            </thead>
            <tbody>
              @foreach ($users as $user)
                <tr>
                  <td>{{$user->name}}</td>
                  <td>{{$user->username}}</td>
                  <td>{{$user->email}}</td>
                  @if ($user->admin_type==1)
                       <td>Admin</td>
                  @else
                      <td>Sub-Admin</td>
                  @endif

                  <td><a href="{{route('editAdmin',[$user->id])}}" class="btn btn-info">Edit</a>
                    {{-- <a href="{{route('deleteAdmin',['1'])}}" class="btn btn-danger">Delete</a></td> --}}
                </tr>
              @endforeach


            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
