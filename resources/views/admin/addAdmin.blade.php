@extends('base.layout.master')

@section('content')
  <div data-widget-group="group1">

  	<div class="panel panel-default" data-widget='{"draggable": "false"}'>
  		<div class="panel-heading">
  			<h2>Add Admin</h2>
  			<div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
  		</div>
  		<div class="panel-editbox" data-widget-controls=""></div>
  		<div class="panel-body">
        {{-- Form Validation errors from server side --}}

        {{-- @if ($errors->count()>0)
          <ul>
            @foreach ($errors->all() as $error)

             <li class="alert alert-warning">{{$error}}</li>
            @endforeach
          </ul>

        @endif --}}
        @if (session()->has('error'))
            <p  class="alert alert-danger">{{session('error')}}</p>
        @endif
        @if (session()->has('success'))
            <p  class="alert alert-success">{{session('success')}}</p>
        @endif
  			<form action="{{route('saveAdmin')}}" class="form-horizontal row-border" method="post" enctype="multipart/form-data" id="myform">
          {{csrf_field()}}
  				<div class="form-group">
  					<label class="col-sm-2 control-label">Name</label>
  					<div class="col-sm-8">
  						<input type="text" name="name" class="form-control"  value="{{old('name')}}" >
              @if ($errors->has('name'))
                <span  style="color:red">{{$errors->first('name')}}</span>
              @endif
  					</div>
  				</div>
          <div class="form-group">
  					<label class="col-sm-2 control-label">User Name</label>
  					<div class="col-sm-8">
  						<input type="text" name="uname" class="form-control" value="{{old('uname')}}">
              @if ($errors->has('uname'))
                <span  style="color:red">{{$errors->first('uname')}}</span>

              @endif
  					</div>
  				</div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-8">
              <input type="password" name="pwd" class="form-control" value="{{old('pwd')}}" >
              @if ($errors->has('pwd'))
                    <span  style="color:red">{{$errors->first('pwd')}}</span>

              @endif
            </div>
          </div>
  				<div class="form-group">
  					<label class="col-sm-2 control-label" >Email</label>
  					<div class="col-sm-8">
  						<input type="text" name="email" class="form-control" value="{{old('email')}}" id="email">
              @if ($errors->has('email'))
                   <span  style="color:red">{{$errors->first('email')}}</span>
              @endif
  					</div>
  				</div>





  			<div class="form-group">
  					<label class="col-sm-2 control-label">Admin Type</label>
  					<div class="col-sm-8">
  						<select class="form-control" id="source" name="admintype" value="{{old('admintype')}}">
  							<optgroup label="">
  								<option value="1">admin</option>
  								<option value="2">sub admin</option>
  							</optgroup>
                @if ($errors->has('admintype'))
                     <span  style="color:red">{{$errors->first('admintype')}}</span>
                @endif
  						</select>
  					</div>
  				</div>

          <div class="form-group">
    					<label class="col-sm-2 control-label">Image</label>
    					<div class="col-sm-8">
    					    <input type="file" name="image" class="form-control" value="{{old('image')}}">
                  @if ($errors->has('image'))
                       <span  style="color:red">{{$errors->first('image')}}</span>
                  @endif
    					</div>
    				</div>


            <div class="col-sm-8 col-sm-offset-2">
              <button type="submit" class="btn-primary btn">Submit</button>
              {{-- <button class="btn-danger btn">Cancel</button> --}}
            </div>



  			</form>

  		</div>
  		<div class="panel-footer">
  		<!--	<div class="row">
  				<div class="col-sm-8 col-sm-offset-2">
  					<button type="submit" class="btn-primary btn">Submit</button>
  					<button class="btn-default btn">Cancel</button>
  				</div>

  			</div>-->
  		</div>
  	</div>
@endsection
@section('custom-script')
  <script type="text/javascript">
    $(document).ready(function() {
       $("#myform").validate({
              rules: {
                "name": "required",
                "uname": "required",
                "email":{
                      required:true,
                      email:true
                },
                "admintype":{
                    required:true,
                  //  digits:true
                },
                 "image":"required",
                 "pwd":"required"
              },
              messages :{
                "name" :"enter name",
                "uname" :"enter username",
                "email": {
                  required:"enter email",
                  email :"enter valid email"
                },
                "admintype":{
                    required:"choose admin type",
                  //  digits:"enter only number"
                },
                 "image" :"choose an image",
                 "pwd" :"enter password"
              },
              errorClass: "my-error-class",
       });
    });
  </script>
  <style media="screen">
    .my-error-class{
      color: red;

    }
  </style>
@endsection
