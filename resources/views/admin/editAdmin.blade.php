@extends('base.layout.master')
@section('content')
  <div data-widget-group="group1">

  	<div class="panel panel-default" data-widget='{"draggable": "false"}'>
  		<div class="panel-heading">
  			<h2>Add Admin</h2>
  			<div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
  		</div>
  		<div class="panel-editbox" data-widget-controls=""></div>
  		<div class="panel-body">
       @foreach ($user as $usr)


  			<form action="{{route('updateAdmin')}}" class="form-horizontal row-border" method="post" enctype="multipart/form-data" id="myform">
          {{csrf_field()}}
  				<div class="form-group">
  					<label class="col-sm-2 control-label">Name</label>
  					<div class="col-sm-8">
  						<input type="text" name="name" value="{{$usr->name}}" class="form-control">
              <input type="hidden" name="id" value="{{$usr->id}}">
  					</div>
  				</div>
          <div class="form-group">
  					<label class="col-sm-2 control-label">User Name</label>
  					<div class="col-sm-8">
  						<input type="text" name="uname" value="{{$usr->username}}" class="form-control">
  					</div>
  				</div>
          {{-- <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-8">
              <input type="password" name="pwd" class="form-control" >
            </div>
          </div> --}}
  				<div class="form-group">
  					<label class="col-sm-2 control-label">Email</label>
  					<div class="col-sm-8">
  						<input type="text" name="email" value="{{$usr->email}}" class="form-control">
  					</div>
  				</div>





  			<div class="form-group">
  					<label class="col-sm-2 control-label">Admin Type</label>
  					<div class="col-sm-8">
  						<select class="form-control" id="source" name="admintype">
  							<optgroup label="">
                @php
                  switch ($usr->admin_type) {
                    case 1:
                  echo '<option value="1" selected>admin</option>
                    <option value="2">sub admin</option>';
                      break;

                    case 2:
                    echo '<option value="1" selected>admin</option>
                      <option value="2" selected>sub admin</option>';
                      break;
                  }
                @endphp




  							</optgroup>

  						</select>
  					</div>
  				</div>
          {{-- <div class="form-group">
    					<label class="col-sm-2 control-label">Image</label>
    					<div class="col-sm-8">
    					    <input type="file" name="image" value="" class="form-control">
    					</div>
    				</div> --}}


            <div class="col-sm-8 col-sm-offset-2">
              <button type="submit" class="btn-primary btn">Update</button>
              {{-- <button class="btn-danger btn">Cancel</button> --}}
            </div>



  			</form>
          @endforeach
  		</div>
  		<div class="panel-footer">
  		<!--	<div class="row">
  				<div class="col-sm-8 col-sm-offset-2">
  					<button type="submit" class="btn-primary btn">Submit</button>
  					<button class="btn-default btn">Cancel</button>
  				</div>

  			</div>-->
  		</div>
  	</div>
@endsection

@section('custom-script')
  <script type="text/javascript">
    $(document).ready(function() {
       $("#myform").validate({
              rules: {
                "name": "required",
                "uname": "required",
                "email":{
                      required:true,
                      email:true
                },
                "admintype":{
                    required:true,
                  //  digits:true
                },
                 "image":"required",
                 "pwd":"required"
              },
              messages :{
                "name" :"enter name",
                "uname" :"enter username",
                "email": {
                  required:"enter email",
                  email :"enter valid email"
                },
                "admintype":{
                    required:"choose admin type",
                  //  digits:"enter only number"
                },
                 "image" :"choose an image",
                 "pwd" :"enter password"
              },
              errorClass: "my-error-class",
       });
    });
  </script>
  <style media="screen">
    .my-error-class{
      color: red;

    }
  </style>
@endsection
