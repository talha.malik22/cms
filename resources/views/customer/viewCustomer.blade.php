@extends('base.layout.master')


@section('content')
  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="panel panel-default" data-widget='{"draggable": "false"}'>
        <div class="panel-heading">
          <h2>View Customer</h2>
          <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
          <div class="options">

          </div>
        </div>
        @if (session()->has('error'))
            <p  class="alert alert-danger">{{session('error')}}</p>
        @endif
        @if (session()->has('success'))
            <p  class="alert alert-success">{{session('success')}}</p>
        @endif
        <div class="panel-body">

          <table class="table table-bordered">
            <thead>
              <tr>

                <th>Name</th>
                <th>Email</th>
                <th>Phone#</th>
                <th>Address</th>
                {{-- <th>Merchant ID</th> --}}
                <th>Status</th>

              </tr>

            </thead>
            <tbody>
              @foreach ($customers as $customer)
                <tr>
                  <td>{{$customer->name}}</td>
                  <td>{{$customer->email}}</td>
                  <td>{{$customer->phone_number}}</td>
                  <td>{{$customer->address}}</td>
                  {{-- <td>{{$product->merchant_id}}</td> --}}

                  @if ($customer->status==1)
                         <td>online/verified</td>
                  @else
                   <td>offline/unverify</td>
                  @endif

                  {{-- <td>
                     <a href="{{route('editCustomer',[$customer->id])}}" class="btn btn-info">Edit</a>
                     <a href="{{route('deleteCustomer',[$customer->id])}}" class="btn btn-danger"  onclick="return confirm('Are you sure you want to Remove?');" >Delete</a>
                  </td> --}}
                </tr>
              @endforeach


            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
