@extends('base.layout.master')

@section('content')
  <div data-widget-group="group1">

  	<div class="panel panel-default" data-widget='{"draggable": "false"}'>
  		<div class="panel-heading">
  			<h2>Add Category</h2>
  			<div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
  		</div>
  		<div class="panel-editbox" data-widget-controls=""></div>
  		<div class="panel-body">
        {{-- Form Validation errors from server side --}}

        {{-- @if ($errors->count()>0)
          <ul>
            @foreach ($errors->all() as $error)

             <li class="alert alert-warning">{{$error}}</li>
            @endforeach
          </ul>

        @endif --}}
        @if (session()->has('error'))
            <p  class="alert alert-danger">{{session('error')}}</p>
        @endif
        @if (session()->has('success'))
            <p  class="alert alert-success">{{session('success')}}</p>
        @endif
  			<form action="{{route('saveCategory')}}" class="form-horizontal row-border" method="post" enctype="multipart/form-data" id="myform">
          {{csrf_field()}}
  				<div class="form-group">
  					<label class="col-sm-2 control-label">Category Name</label>
  					<div class="col-sm-8">
  						<input type="text" name="name" class="form-control" value="{{old('name')}}">
              @if ($errors->has('name'))
                <span  style="color:red">{{$errors->first('name')}}</span>
              @endif
  					</div>
  				</div>
          <div class="form-group">
              <label class="col-sm-2 control-label"> Parent Category</label>
              <div class="col-sm-8">
                <select class="form-control" id="source" name="p_category" value="{{old('p_category')}}">
                  <option value="0">Select Parent Category</option>
                  @foreach ($categories as $cate)

                      <option value="{{$cate->id}}">{{$cate->name}}</option>

                  @endforeach

                  @if ($errors->has('p_category'))
                       <span  style="color:red">{{$errors->first('p_category')}}</span>
                  @endif
                </select>
              </div>
            </div>

            <div class="form-group">
  					<label class="col-sm-2 control-label">Image</label>
  					<div class="col-sm-8">
  						<input type="file" name="photo" class="form-control" value="{{old('photo')}}">
              @if ($errors->has('name'))
                <span  style="color:red">{{$errors->first('photo')}}</span>
              @endif
  					</div>
  				</div>


            <div class="col-sm-8 col-sm-offset-2">
              <button type="submit" class="btn-primary btn">Add Category</button>
               {{-- <button class="btn-danger btn">Cancel</button> --}}
            </div>



  			</form>

  		</div>
  		<div class="panel-footer">
  		<!--	<div class="row">
  				<div class="col-sm-8 col-sm-offset-2">
  					<button type="submit" class="btn-primary btn">Submit</button>
  					<button class="btn-default btn">Cancel</button>
  				</div>

  			</div>-->
  		</div>
  	</div>
@endsection
@section('custom-script')
   <script type="text/javascript">
     $(document).ready(function() {
        $("#myform").validate({
               rules: {
                 "name": "required",
                 "photo": "required",
                 "p_category" :{
                  // required:true
                 }
               },
               messages :{
                 "name" :"enter name",
                 "photo" :"please select image",
                 "p_category" :{
                //   required:"select Category"
                 }
               },
               errorClass: "my-error-class",
        });
     });
   </script>
   <style media="screen">
     .my-error-class{
       color: red;
     }
   </style>
@endsection
