@extends('base.layout.master')


@section('content')
  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="panel panel-default" data-widget='{"draggable": "false"}'>
        <div class="panel-heading">
          <h2>View Category</h2>
          <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
          <div class="options">

          </div>
        </div>
        @if (session()->has('message'))
         <p class="alert alert-success">{{session('message')}}</p>
        @endif
        @if (session()->has('error'))
            <p  class="alert alert-danger">{{session('error')}}</p>
        @endif
        @if (session()->has('success'))
            <p  class="alert alert-success">{{session('success')}}</p>
        @endif
        <div class="panel-body">

          <table class="table table-bordered">
            <thead>
              <tr>

                <th>Name</th>
                <th>Enable/Disable Featured</th>
                <th>Action</th>


              </tr>

            </thead>
            <tbody>
              @foreach ($categories as $category)
                <tr>
                  <td>{{$category->name}}</td>
                  @if ($category->is_featured==1)
                         <td><input type="checkbox" name="isfeatured" value="{{$category->id}}" class="check"  checked data-toggle="toggle" style="height:120px"></td>
                  @else
                     <td><input type="checkbox" name="isfeatured" value="{{$category->id}}" class="check" data-toggle="toggle"></td>
                  @endif


                  <td>
                     <a href="{{route('editCategory',[$category->id])}}" class="btn btn-info">Edit</a>
                     {{-- <a href="{{route('deleteCategory',[$category->id])}}" class="btn btn-danger"  onclick="return confirm('Are you sure you want to Remove?');" >Delete</a> --}}
                  </td>
                </tr>
              @endforeach


            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('custom-script')
 <script type="text/javascript">
    $(document).ready(function() {
      $(".check").change(function(event) {
          if (this.checked) {
          console.log($(this).val());
          var id=$(this).val();
        //  console.log(idddd);
          //var n = idddd.toString();
          $.get("/category/isFeatured/"+id+"/"+1,function(data, status){
               alert(data);
           });

          }
          else {
          //  alert('test');
              var id=$(this).val();
            $.get("/category/isFeatured/"+id+"/"+0,function(data, status){
                 alert(data);
             });
          }
      });
    });
 </script>
@endsection
