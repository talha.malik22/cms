@extends('base.layout.master')

@section('content')
  <div data-widget-group="group1">

  	<div class="panel panel-default" data-widget='{"draggable": "false"}'>
  		<div class="panel-heading">
  			<h2>Edit Product</h2>
  			<div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
  		</div>
  		<div class="panel-editbox" data-widget-controls=""></div>
  		<div class="panel-body">
        {{-- Form Validation errors from server side --}}

        {{-- @if ($errors->count()>0)
          <ul>
            @foreach ($errors->all() as $error)

             <li class="alert alert-warning">{{$error}}</li>
            @endforeach
          </ul>

        @endif --}}
        @if (session()->has('success'))
            <p  class="alert alert-success">{{session('success')}}</p>
        @endif
        @if (session()->has('error'))
            <p  class="alert alert-danger">{{session('error')}}</p>
        @endif
        <form action="{{route('updateProduct')}}" class="form-horizontal row-border" method="post" id="myform">
          {{csrf_field()}}
        @foreach ($products as $item)



  				<div class="form-group">
  					<label class="col-sm-2 control-label">Name</label>
  					<div class="col-sm-8">
  						<input type="text" name="name" class="form-control" value="{{$item->name}}">
              <input type="hidden" name="id" class="form-control" value="{{$item->id}}">
              {{-- @if ($errors->has('name'))
                <span  style="color:red">{{$errors->first('name')}}</span>
              @endif --}}
  					</div>
  				</div>

          <div class="form-group">
              <label class="col-sm-2 control-label">Category</label>
              <div class="col-sm-8">
                <select class="form-control" id="source" name="category" value="{{old('category')}}">
                  @foreach ($category as $cate)
                      @if ($cate->id==$item->category_id)
                            <option value="{{$cate->id}}" selected>{{$cate->name}} </option>
                      @else
                          <option value="{{$cate->id}}">{{$cate->name}}</option>
                      @endif


                  @endforeach

                  @if ($errors->has('category'))
                       <span  style="color:red">{{$errors->first('category')}}</span>
                  @endif
                </select>
              </div>
            </div>

          <div class="form-group">
  					<label class="col-sm-2 control-label">Cost Price</label>
  					<div class="col-sm-8">
  						<input type="text" name="cost_price" class="form-control" value="{{$item->cost_price}}">
              {{-- @if ($errors->has('cost_price'))
                <span  style="color:red">{{$errors->first('cost_price')}}</span>

              @endif --}}
  					</div>
  				</div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Sale Price</label>
            <div class="col-sm-8">
              <input type="text" name="sale_price" class="form-control" value="{{$item->sale_price}}">
              {{-- @if ($errors->has('sale_price'))
                <span  style="color:red">{{$errors->first('sale_price')}}</span>

              @endif --}}
            </div>
          </div>

          {{-- <div class="form-group">
    					<label class="col-sm-2 control-label">Merchant</label>
    					<div class="col-sm-8">
    						<select class="form-control" id="source" name="merchant" value="{{$item->merchant_id}}">

    								<option value="1">Nazir</option>
    								<option value="2">Ali</option>

                  @if ($errors->has('merchant'))
                       <span  style="color:red">{{$errors->first('merchant')}}</span>
                  @endif
    						</select>
    					</div>
    				</div> --}}

            <div class="form-group">
              <label class="col-sm-2 control-label">Discount Percentage</label>
              <div class="col-sm-8">
                <input type="text" name="discount" class="form-control" value="{{$item->discount_perc}}">
                {{-- @if ($errors->has('discount'))
                  <span  style="color:red">{{$errors->first('discount')}}</span>

                @endif --}}
              </div>
            </div>


          {{-- <div class="form-group">
    					<label class="col-sm-2 control-label">Image</label>
    					<div class="col-sm-8">
    					    <input type="file" name="image" class="form-control" value="{{old('image')}}">
                  @if ($errors->has('image'))
                       <span  style="color:red">{{$errors->first('image')}}</span>
                  @endif
    					</div>
    				</div> --}}

            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-8">
                    <textarea name="desc" rows="8" cols="80" class="form-control">{{$item->desc}}</textarea>
                    {{-- @if ($errors->has('desc'))
                         <span  style="color:red">{{$errors->first('desc')}}</span>
                    @endif --}}
                </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-2 control-label">Specification</label>
                  <div class="col-sm-8">
                      <textarea name="desc" rows="8" cols="80" class="form-control">{{$item->specification}}</textarea>
                      {{-- @if ($errors->has('desc'))
                           <span  style="color:red">{{$errors->first('desc')}}</span>
                      @endif --}}
                  </div>
                </div>
            <div class="col-sm-8 col-sm-offset-2">
              <button type="submit" class="btn-primary btn">Update</button>
              {{-- <button class="btn-danger btn">Cancel</button> --}}
            </div>




          @endforeach
          	</form>

  		</div>
  		<div class="panel-footer">
  		<!--	<div class="row">
  				<div class="col-sm-8 col-sm-offset-2">
  					<button type="submit" class="btn-primary btn">Submit</button>
  					<button class="btn-default btn">Cancel</button>
  				</div>

  			</div>-->
  		</div>
  	</div>
@endsection

@section('custom-script')
  <script type="text/javascript">
    $(document).ready(function() {
       $("#myform").validate({
              rules: {
                "name": "required",
                "category":{
                      required:true,
                    //  email:true
                },
                "cost_price":{
                    required:true,
                    digits:true
                },

                "sale_price":{
                    required:true,
                    digits:true
                },
                "discount":{
                    required:true,
                    digits:true
                },
                 "image[]":"required",
                 "desc":"required",
                 "specification":"required",
              },
              messages :{
                "name" :"enter name",
                "category": {
                  required:"choose category",
                //  email :"enter valid email"
                },
                "cost_price":{
                    required:"enter cost price",
                    digits:"must be numeric"
                },

                "sale_price":{
                  required:"enter sale price",
                  digits:"must be numeric"
                },
                "discount":{
                  required:"enter discount",
                  digits:"must be numeric"
                },
                 "image[]":"select image",
                 "desc":"enter description",
                 "specification":"enter specification",
              },

              errorClass: "my-error-class",
       });
    });
  </script>
  <style media="screen">
    .my-error-class{
      color: red;

    }
  </style>
@endsection
