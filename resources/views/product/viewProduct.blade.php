@extends('base.layout.master')


@section('content')
  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="panel panel-default" data-widget='{"draggable": "false"}'>
        <div class="panel-heading">
          <h2>View Product</h2>
          <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
          <div class="options">

          </div>
        </div>
        @if (session()->has('success'))
         <p class="alert alert-success">{{session('success')}}</p>
        @endif
        @if (session()->has('error'))
         <p class="alert alert-danger">{{session('error')}}</p>
        @endif
        <div class="panel-body">

          <table class="table table-bordered">
            <thead>
              <tr>

                <th>Name</th>
                <th>Category ID</th>
                <th>Cost Price</th>
                <th>Sale Price</th>
                {{-- <th>Merchant ID</th> --}}
                <th>Discount</th>
                <th>Action</th>
              </tr>

            </thead>
            <tbody>
              @foreach ($products as $product)
                <tr>
                  <td>{{$product->name}}</td>
                  <td>{{$product->category_id}}</td>
                  <td>{{$product->cost_price}}</td>
                  <td>{{$product->sale_price}}</td>
                  {{-- <td>{{$product->merchant_id}}</td> --}}
                  <td>{{$product->discount_perc}}%</td>
                  <td>
                     <a href="{{route('editProduct',[$product->id])}}" class="btn btn-info">Edit</a>
                     <a href="{{route('deleteProduct',[$product->id])}}" class="btn btn-danger"  onclick="return confirm('Are you sure you want to Remove?');" >Delete</a>
                  </td>
                </tr>
              @endforeach


            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
