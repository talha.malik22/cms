<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// User Login section
// Login page
Route::get('/','AdminLoginControleler@login');
// Authenticate user
Route::post('login/authenticate/user','AdminLoginControleler@authenticateUser')->name('authenticateUser');


// Admin Section
// main page of admin pannel
Route::get('admin/index','AdminController@index')->name('adminindex');
Route::get('admin/dashboard','AdminController@dashboard')->name('adminDashboard');
Route::get('admin/addAdmin','AdminController@addAdmin')->name('addNewAdmin');
Route::post('admin/save','AdminController@saveAdmin')->name('saveAdmin');
Route::get('admin/view','AdminController@viewAdmin')->name('viewAdmin');
Route::get('admin/delete/{id}','AdminController@deleteAdmin')->name('deleteAdmin');
Route::get('admin/edit/{id}','AdminController@editAdmin')->name('editAdmin');
Route::post('admin/edit/update','AdminController@updateAdmin')->name('updateAdmin');

//  Customer Section
Route::get('customer/add','CustomerController@addCustomer')->name('addCustomer');
Route::post('customer/save','CustomerController@saveCustomer')->name('saveCustomer');
Route::get('customer/view','CustomerController@viewcCustomer')->name('viewCustomer');
Route::post('customer/signup','CustomerController@customerSignup')->name('signup');
Route::post('customer/signin','CustomerController@customerLogin')->name('signin');
Route::post('customer/social','CustomerController@socialLogin')->name('social');


// Category section
Route::get('category/add','CategoryController@addCategory')->name('addCategory');
Route::post('category/save','CategoryController@saveCategory')->name('saveCategory');
Route::get('category/view','CategoryController@viewCategory')->name('viewCategory');
Route::get('category/view/subcategory','CategoryController@displaysubCategory')->name('viewsubcategory');
Route::get('category/edit/{id}','CategoryController@editCategory')->name('editCategory');
Route::get('category/edit/subcategory/{id}','CategoryController@editSubcategory')->name('editSubcategory');
Route::get('category/getChildCategory/{id}','CategoryController@subCategory')->name('getSubCategory');
Route::post('category/update','CategoryController@updateSubcategory')->name('updateSubCategory');
Route::post('category/update/subcategory','CategoryController@updateCategory')->name('updateCategory');
Route::get('category/isFeatured/{id}/{status?}','CategoryController@isFeatured')->name('isFeatured');
Route::get('category/search/{seacrh}','CategoryController@searchCategory');
// Route::get('category/delete/{id}','CategoryController@deleteCategory')->name('deleteCategory');

/*  category Api Routes */
Route::get('category/getAll','CategoryController@getAllCategories');
Route::get('category/featured','CategoryController@featuredCaterories');
Route::get('subCategory/getAll','CategoryController@subCategories');
Route::get('subCategory/{categoryId}','CategoryController@subCatByCategoryId');




// Product Section
Route::get('product/add','ProductController@addProduct')->name('addProduct');
Route::post('product/save','ProductController@saveProduct')->name('saveProduct');
Route::get('product/view','ProductController@viewProduct')->name('viewProduct');
Route::get('product/edit/{id}','ProductController@editProduct')->name('editProduct');
Route::post('product/update','ProductController@updateProduct')->name('updateProduct');
Route::get('product/delete/{id}','ProductController@deleteProduct')->name('deleteProduct');

/***** APi routes  ****/
Route::get('product/{subCatId}','ProductController@productBySubCategoryId');
Route::get('product/productId/{productId}','ProductController@productById');
