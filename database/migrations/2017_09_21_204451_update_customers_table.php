<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return voirozn
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->text('uid')->nullable();
            $table->string('enviroment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('customers', function (Blueprint $table) {
       $table->dropColumn('uid','enviroment');
      });
    }
}
