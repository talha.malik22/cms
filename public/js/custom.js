/**
 * Created by HP on 2/17/2017.
 */
function loadUsertoModal(id,fee,bal){
    $("#user_id").val(id);
    $("#user_fee").val(fee);
    $("#amount_rem").val(bal);
}
$(document).ready(function(){
    $("#amount_received").on('blur',function(){
        var paid_amt = $("#amount_received").val();
        var total_fee = $("#user_fee").val();
        var prev_bal = $("#amount_rem").val();
        var bal = (parseInt(total_fee) - parseInt(paid_amt)) + parseInt(prev_bal);
        $("#amount_rem").val(bal);
    });

    $("#close_modal_btn, .close").on("click",function(){
        $("#amount_received").val("");
        $("#amount_rem").val("");
    });

    $("#save_invoice_btn").on("click",function(){
        var frm_data = {};
        frm_data.amount_rec = $("#amount_received").val();
        frm_data.balance = $("#amount_rem").val();
        frm_data.rec_by = $("#received_by").val();
        frm_data.date_rec = $("#date_rec").val();
        frm_data.user_id = $("#user_id").val();
        frm_data.user_fee = $("#user_fee").val();
        frm_data.inv_num = $("#inv_num").val();

        $.ajax({
            url: "/customer/createinvoice",
            data: frm_data
        }).done(function() {
            location.reload();
        });
    });


});
