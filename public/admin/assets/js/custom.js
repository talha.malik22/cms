$("#p_category").on('change',function(){
	var cat_id = $("#p_category").val();
	$.get("/category/getChildCategory/"+cat_id, function(data){
            var res = JSON.parse(data);
            var sub_cat_html = "<option value='0'>Select Sub Category</option>";
            for(var i=0; i<res.length;i++){
        		sub_cat_html += "<option value='"+res[i].id+"'>"+res[i].name+"</option>"
            }
            $("#c_category").html(sub_cat_html);
            $("#sub_cat_section").removeClass("dis-none").addClass("dis-block");

        });
});